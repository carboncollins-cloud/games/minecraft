# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)

## [Unreleased]

## [2022-05-08]
### Added
- placeholder for CSI volume

### Changed
- Moved workload to game-servers namespace 

## [2022-05-07]

### Changed
- Afinity to constraint

## [2022-04-26]

### Changed
- World reset
- Increased max memory to 8GB
- Move minecraft data to CSI volume

## [2022-04-24]

### Added
- Job name to nomad job file
- Namespace to nomad job file

### Changed
- Increased CPU to 4.2GHz

## [2022-04-23]
### Changed
- Updated all links to point to new repository location
- Moved repository to [CarbonCollins - Cloud](https://gitlab.com/carboncollins-cloud) GitLab Group
