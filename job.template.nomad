job "game-minecraft" {
  name = "Game Server (Minecraft)"
  type = "service"
  region = "global"
  datacenters = ["proxima"]
  namespace = "game-servers"

  vault {
    policies = ["job-game-minecraft"]
  }

  group "minecraft" {
    count = 1

    constraint {
      attribute = "${attr.unique.hostname}"
      value     = "proxima-g"
      // weight    = 75
    }

    volume "minecraft-data" {
      type = "csi"
      source = "minecraft-data"
      read_only = false

      attachment_mode = "file-system"
      access_mode = "single-node-writer"
    }

    network {
      port "minecraft" {
        static = 25565
      }

      port "rcon" {}
    }

    task "minecraft" {
      driver = "docker"

      kill_timeout = "90s"
      kill_signal = "SIGTERM"

      volume_mount {
        volume = "minecraft-data"
        destination = "/data"
        read_only = false
      }

      config {
        image = "itzg/minecraft-server:latest"

        ports = ["minecraft"]
      }

      template {
        data = <<EOH
[[ fileContents "./config/minecraft.template.env" ]]
        EOH

        destination = "secrets/minecraft.env"
        env = true
      }

      env {
        TZ="Europe/Stockholm"
        UID=[[ .defaultUserId ]]
        GID=[[ .defaultGroupId ]]
      }

      resources {
        cpu = 4200
        memory = 8192
      }

      service {
        name = "minecraft"
        port = "minecraft"

        check {
          name = "Minecraft"
          port = "minecraft"
          type = "tcp"
          interval = "30s"
          timeout = "10s"
          task = "minecraft"
        }
      }
    }
  }

  reschedule {
    delay = "10s"
    delay_function = "exponential"
    max_delay = "10m"
    unlimited = true
  }

  update {
    health_check = "checks"
    min_healthy_time = "10s"
    healthy_deadline = "10m"
    progress_deadline = "15m"
    auto_revert = true
  }

  meta {
    gitSha = "[[ .gitSha ]]"
    gitBranch = "[[ .gitBranch ]]"
    pipelineId = "[[ .pipelineId ]]"
    pipelineUrl = "[[ .pipelineUrl ]]"
    projectId = "[[ .projectId ]]"
    projectUrl = "[[ .projectUrl ]]"
    statefull = "true"
  }
}
